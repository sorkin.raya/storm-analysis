import pandas as pd
import numpy as np
from matplotlib import pyplot as pp
import main


def _get_noise(num_points, coord_range):
    x_noise_zero = np.round(np.random.normal(0, coord_range / 2, num_points)).astype(np.int)
    x_noise_zero[x_noise_zero < -coord_range] = -coord_range
    x_noise_zero[x_noise_zero > coord_range] = coord_range
    return x_noise_zero


PIXEL_SIZE = 1
COORD_RANGE = 1000


def test_mask():
    errors = []
    for i in range(10):
        x, y = np.mgrid[-COORD_RANGE:(COORD_RANGE + 1), -COORD_RANGE:(COORD_RANGE + 1)]
        circle = (x**2 + y**2) < (COORD_RANGE ** 2) / 2
        circle[
            _get_noise((COORD_RANGE ** 2) * 10, COORD_RANGE) + COORD_RANGE,
            _get_noise((COORD_RANGE ** 2) * 10, COORD_RANGE) + COORD_RANGE] = 0
        circle[
            _get_noise(COORD_RANGE * 20, COORD_RANGE) + COORD_RANGE,
            _get_noise(COORD_RANGE * 20, COORD_RANGE) + COORD_RANGE] = 1
        particle_y, particle_x = np.where(circle)
        data = pd.DataFrame({'x': particle_x.reshape(-1), 'y': particle_y.reshape(-1)})
        mask = main.cell_mask(data, PIXEL_SIZE)
        errors.append(np.sum(mask != ((x**2 + y**2) < (COORD_RANGE ** 2) / 2)) / (0.95 * 4 * COORD_RANGE ** 2))
    assert np.all(np.array(errors) < 0.05)


def test_count_particles():
    data = pd.DataFrame({
        'x': list(range(10000)), 'y': [min(10000, (x / 100) ** 2) for x in range(10000)],
        'frame': np.round(np.linspace(0, 1000, num=10000))
    })
    count = main.get_particle_density(data, np.ones((10000, 10000)))
    assert count.shape == (1001, 1001)


def test_density_over_time_and_peak_to_peak_diff():
    data = pd.DataFrame({
        'x': [7000 + 10000 * np.cos(x) + np.random.normal(0, 1000) for x in np.linspace(0, 2*np.pi, 1000000)],
        'y': [7000 + 10000 * np.sin(x) + np.random.normal(0, 1000) for x in np.linspace(0, 2*np.pi, 1000000)],
        'frame': np.round(np.linspace(0, 1000, num=1000000))
    })
    peak_distances_over_time, histograms, x, probs = main.density_over_time_and_peak_to_peak_diff(data)
    #assert peak_distances_over_time < 2
    pp.plot(peak_distances_over_time)
    pp.show()
    pp.plot(histograms[1][1:], histograms[0])
    pp.show()
    pp.plot(x, probs)
    pp.show()

