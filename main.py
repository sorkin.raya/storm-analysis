import pandas as pd
import numpy as np
from scipy.ndimage import morphology, gaussian_filter, convolve, binary_fill_holes
from scipy import stats, spatial
from scipy.misc import imsave, imread
from skimage.feature import peak_local_max
import os

from matplotlib import pyplot as pp

import itertools

NUM_FRAMES_TO_INTEGRATE = 150

PIXEL_SIZE = 10  # in nm
WINDOW_SIZE = 10  # in pixels



def get_particle_density(data, mask):
    count = np.zeros((round(data.x.max() / PIXEL_SIZE).astype(np.int) + 1, round(data.y.max() / PIXEL_SIZE).astype(np.int) + 1))
    for x, y in zip(data.x, data.y):
        x = int(round(x / PIXEL_SIZE))
        y = int(round(y / PIXEL_SIZE))
        if mask[x, y] == 0:
            continue
        count[x, y] += 1
    circular_kernel = circle(WINDOW_SIZE / 2)
    return convolve(count, circular_kernel) / (np.sum(circular_kernel))


def circle(radius):
    x, y = np.mgrid[-radius:radius + 1, -radius:radius + 1]
    return x**2 + y**2 <= radius**2


def cell_mask(data, pixel_size):
    mask = np.zeros(
        (
            round(data.x.max() / pixel_size).astype(np.int) + 1,
            round(data.y.max() / pixel_size).astype(np.int) + 1))
    mask[
        np.round(data.x.values / pixel_size).astype(np.int),
        np.round(data.y.values / pixel_size).astype(np.int)] = 1
    binary_square = morphology.generate_binary_structure(2, 1)
    mask = morphology.binary_closing(mask, binary_square, 20)
    mask = morphology.binary_opening(mask, binary_square, 80)
    mask = morphology.binary_closing(mask, binary_square, 20)
    mask = gaussian_filter(mask.astype(np.float), 40)
    return binary_fill_holes(mask > 0.5)


def particle_densities_in_time_chunks(data, mask, num_frames_to_integrate):
    num_of_frames = data.frame.max().astype(np.int)
    for t_idx in range(1, int(round(num_of_frames / num_frames_to_integrate))):
        temporal_chunk = data[
            (data.frame < t_idx * num_frames_to_integrate) & (data.frame > (t_idx - 1) * num_frames_to_integrate)
            ]
        yield get_particle_density(temporal_chunk, mask)


def nearest_neighbour_distances(pts1, pts2):
    if pts2 is None or pts1 is None or not pts2.size or not pts1.size:
        return np.array([])
    kd_tree = spatial.KDTree(pts1)
    if pts1 is not pts2:
        distances, _ = kd_tree.query(pts2)
        return distances
    distances, _ = kd_tree.query(pts2, k=2)
    return distances[:, 1]
    #for x1, y1 in pts1:
    #    min_dist = np.inf
    #    for x2, y2 in pts2:
    #        min_dist = min(min_dist, (x1 - x2) ** 2 + (y1 - y2) ** 2)
    #    yield math.sqrt(min_dist)


def save_frames(data, mask, num_frames_to_integrate):
    new_path = os.path.join(os.path.curdir, str(num_frames_to_integrate))
    os.mkdir(new_path)
    for idx, particle_density in enumerate(particle_densities_in_time_chunks(data, mask, num_frames_to_integrate)):
        imsave(f'{new_path}/particle_density_{idx:04d}.png', particle_density)


def density_over_time_and_peak_to_peak_diff(data):
    mask = cell_mask(data, PIXEL_SIZE)
    assert np.sum(mask) > 0
    imsave('mask.png', mask.astype(np.float))
    imsave('total density.png', get_particle_density(data, mask))
    previous_peaks = None
    peak_distances_over_time = []
    histograms = []
    probs = []
    peak_distance_ratios = []
    histograms_of_peak_ratios = []
    x = np.arange(0, 0.2, 0.0002)
    histograms_of_in_frame_peak_distances = []
    for idx, particle_density in enumerate(particle_densities_in_time_chunks(data, mask, NUM_FRAMES_TO_INTEGRATE)):
        imsave(f'particle_density_{idx}.png', particle_density)
        peaks = peak_local_max(
            particle_density, min_distance=int(round(WINDOW_SIZE / 2)), num_peaks=500, threshold_rel=0.01)
        assert peaks.size > 0
        peak_to_prev_peak_distances = nearest_neighbour_distances(previous_peaks, peaks)
        peak_to_peak_distances = nearest_neighbour_distances(peaks, peaks)
        previous_peaks = peaks
        histograms.append(np.histogram(particle_density, bins=40, range=(0, 0.5), density=True))
        kernel = stats.gaussian_kde(particle_density.reshape(-1))
        probs.append(kernel.pdf(x))
        if not peak_to_prev_peak_distances.size:
            continue
        histograms_of_in_frame_peak_distances.append(
            np.histogram(peak_to_peak_distances, bins=100, range=(0.01, 500), density=True))
        histograms_of_peak_ratios.append(
            np.histogram(peak_to_prev_peak_distances / peak_to_peak_distances, bins=100, range=(0.01, 10), density=True))
        peak_distances_over_time.append(
            sum([
                peak_to_prev_distance / peak_to_peak_distance for peak_to_prev_distance, peak_to_peak_distance in zip(
                    peak_to_prev_peak_distances, peak_to_peak_distances)
            ]) / len(peak_to_peak_distances))
        #temp_ratios = np.empty((500,))
        #temp_ratios.fill(np.nan)
        #peak_distance_ratio = peak_to_prev_peak_distances / peak_to_peak_distances
        #temp_ratios[:peak_distance_ratio.size] = peak_distance_ratio
        peak_distance_ratios.append(peak_to_prev_peak_distances / peak_to_peak_distances)
    return (peak_distances_over_time, np.hstack(peak_distance_ratios), histograms, x, probs,
            np.vstack(histograms_of_in_frame_peak_distances), np.vstack(histograms_of_peak_ratios))


def write_density_histogram(data, mask, file_name):
    frame_density_histograms = []
    top_10_percentile_densities = []
    for img in particle_densities_in_time_chunks(data, mask, NUM_FRAMES_TO_INTEGRATE):
        print(np.max(img[img > 0]), np.quantile(img[img > 0], 0.9))
        top_10_percentile_densities.append(np.quantile(img, 0.9))
        density_histogram, _ = np.histogram(img[img > 0].flatten(), bins=60, range=(0, 0.75), density=True)
        frame_density_histograms.append(density_histogram)
    print(np.median(top_10_percentile_densities))
    histogram_over_time = np.vstack(frame_density_histograms).astype(np.float64)
    np.savetxt(f'{file_name}_density_histogram_count_over_time_60_bins.csv', histogram_over_time, delimiter=',')


def write_peak_histogram(data, file_name):
    peak_distances_over_time, peak_distance_ratios, histograms, x, probs, histograms_of_in_frame_peak_distances, histograms_of_peak_ratios = density_over_time_and_peak_to_peak_diff(
        data)
    # for idx, (histogram, prob) in enumerate(zip(histograms, probs)):
    #    pd.DataFrame({
    #        'bin_center': histogram[1][1:] - histogram[1][1] / 2, 'count': histogram[0]}).to_csv(f'histogram_{idx}.csv')
    #    pd.DataFrame({'density': x, 'probability density': prob}).to_csv(f'pdf_{idx}.csv')
    np.savetxt(f'{file_name}_peak_distances.csv', np.vstack(histograms_of_in_frame_peak_distances), delimiter=',')
    np.savetxt(f'{file_name}_peak_ratios.csv', np.vstack(histograms_of_peak_ratios), delimiter=',')

    #pd.DataFrame({'peak_to_peak_distance_ratio': peak_distances_over_time}).to_csv('peak_to_peak_over_time.csv')
    #pd.DataFrame({'peak_distance_ratio': peak_distance_ratios}).to_csv('peak_distance_ratios.csv')
    #kernel = stats.gaussian_kde(peak_distance_ratios)
    #pd.DataFrame({
    #    'peak_distance_ratio': np.arange(0, 1000, 0.1),
    #    'probability': kernel.pdf(np.arange(0, 1000, 0.1))}).to_csv('peak_distance_ratio_pdf.csv')


def process_all_csv_in_current_dir():
    for file_name in os.listdir('.'):
        if not file_name.endswith('.csv'):
            continue
        data = pd.read_csv(file_name)
        mask = cell_mask(data, PIXEL_SIZE)
        write_density_histogram(data, mask, file_name)
        write_peak_histogram(data, file_name)

def fix_column_names(data):
    new_columns = []
    for column in data.columns:
        if column.startswith('x '):
            new_columns.append('x')
        elif column.startswith('y '):
            new_columns.append('y')
        else:
            new_columns.append(column)
    data.columns = new_columns


if __name__ == '__main__':
    file_name = 'cell5 internal control.csv'
    data = pd.read_csv(file_name)
    fix_column_names(data)
    mask = cell_mask(data, PIXEL_SIZE)
    write_density_histogram(data, mask, file_name)
